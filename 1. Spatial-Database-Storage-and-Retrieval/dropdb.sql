drop table FIREBUILDING;
-------------------------------

DELETE USER_SDO_GEOM_METADATA
WHERE TABLE_NAME = 'FIREHYDRANT'
AND COLUMN_NAME = 'SHAPE';

drop table FIREHYDRANT;
-------------------------------------


DELETE USER_SDO_GEOM_METADATA
WHERE TABLE_NAME = 'BUILDING'
AND COLUMN_NAME = 'SHAPE';

DROP TABLE BUILDING;
------------------------------------------------