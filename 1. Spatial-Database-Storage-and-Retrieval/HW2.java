import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Struct;

import javax.naming.spi.DirStateFactory.Result;

import oracle.net.aso.q;
import oracle.security.o3logon.O3LoginClientHelper;
import oracle.spatial.geometry.JGeometry;
import oracle.sql.STRUCT;


public class HW2 {
	@SuppressWarnings("deprecation")
	public static void main(String args[]) throws NumberFormatException, IOException, SQLException{

		File inputFile = new File("E:\\2nd_Sem\\DB\\Homework\\2\\JDBCProj\\src\\hydrant.xy");
		//File inputFile = new File("E:\\2nd_Sem\\DB\\Homework\\2\\JDBCProj\\src\\firebuilding.txt");
		//File inputFile = new File("E:\\2nd_Sem\\DB\\Homework\\2\\JDBCProj\\src\\building.xy");
		FileReader fr = new FileReader(inputFile);
		BufferedReader br = new BufferedReader(fr);
		
		String line;
		StringBuilder queryString = new StringBuilder();
/****************************************Connecting to the Database using JDBC******************************************************/		
		System.out.println("-------- Oracle JDBC Connection Testing ------");
		
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
		}
		catch(Exception e){
			System.out.println("Where is the driver??");
			e.printStackTrace();
		}
		
		System.out.println("Oracle Driver Registered");
		
		Connection conn = null;
		Statement statement;
		
		try{
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "system","Pratik");
		}
		catch(SQLException e){
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
		}
		
		if(conn!=null)
			System.out.println("You made it, take control your database now!");
		else
			System.out.println("Failed to make connection!");
/****************************************Connection to the Database using JDBC completed***********************************************/

/****************************************Insertion of queries started****************************************************************/		
		/*while((line=br.readLine())!=null){
			queryString = new StringBuilder();
			String inputArray[] = line.split(", ");
			queryString.append("INSERT INTO FIREHYDRANT VALUES('");
			queryString.append(inputArray[0]).append("',").append(" SDO_GEOMETRY(2001, NULL, SDO_POINT_TYPE(").append(inputArray[1]).append(",").append(inputArray[2]).append(",NULL), NULL, NULL))");
			System.out.println(queryString);
			statement = conn.createStatement();
			statement.executeUpdate(queryString.toString());
		}*/
		
		/*while((line=br.readLine())!=null){
			queryString = new StringBuilder();
			queryString.append("INSERT INTO FIREBUILDING VALUES('");
			queryString.append(line);
			queryString.append("')");
			statement = conn.createStatement();
			statement.execute(queryString.toString());
			System.out.println(queryString);
		}*/
		
		/*while((line=br.readLine())!=null){
			queryString = new StringBuilder();
			String inputArray[] = line.split(", ");
			//System.out.println(inputArray[1]);
			//System.out.print("Insert into building values ("+(char)39+inputArray[0]+(char)39+", "+(char)39+inputArray[1]+(char)39+", "+inputArray[2]+", "+"SDO_GEOMETRY(2003, NULL, NULL, SDO_ELEM_ARRAY(1, 1003, 1), SDO_ORDINATE_ARRAY(");
			queryString.append("Insert INTO building VALUES (");
			queryString.append((char)39+inputArray[0]+(char)39);
			queryString.append(", ");
			queryString.append((char)39+inputArray[1]+(char)39);
			queryString.append(", ");
			queryString.append(inputArray[2]);
			queryString.append(", ");
			queryString.append("SDO_GEOMETRY(2003, NULL, NULL, SDO_ELEM_INFO_ARRAY(1, 1003, 1), SDO_ORDINATE_ARRAY(");
			//queryString+="Insert INTO building VALUES ("+(char)39+inputArray[0]+(char)39+", "+(char)39+inputArray[1]+(char)39+", "+inputArray[2]+", "+"SDO_GEOMETRY(2003, NULL, NULL, SDO_ELEM_INFO_ARRAY(1, 1003, 1), SDO_ORDINATE_ARRAY(";
			/*for(int i=1, k=3;i<=2*Integer.parseInt(inputArray[2]);i++, k++){
				queryString.append(inputArray[k]);
				queryString.append(", "); 
				//queryString+= inputArray[k]+", ";
			}
			for(int i=3;i<inputArray.length;i+=2){
				queryString.append(inputArray[i]).append(",");
				queryString.append(580-Integer.parseInt(inputArray[i+1])).append(",");
			}
			
			queryString.append(inputArray[3]);
			queryString.append(",");
			queryString.append(580-Integer.parseInt(inputArray[4]));
			queryString.append(")))");
			//queryString+=inputArray[3]+", "+inputArray[4]+")))";
			
			//String query = "select * from dual";
			System.out.println(queryString);
			statement = conn.createStatement();
			statement.executeUpdate(queryString.toString());
			//System.out.println(statement.execute(queryString.toString()));
			//statement.executeQuery("Insert INTO building VALUES (b0, PSA, 12, SDO_GEOMETRY(2003, NULL, NULL, SDO_ELEM_INFO_ARRAY(1, 1003, 1), SDO_ORDINATE_ARRAY(79,512,184,455,179,447,189,442,139,351,131,355,127,347,21,405,26,412,18,417,67,507,74,504,79,512)));");
			//System.out.println("nRecord Inserted Successfully");
		}*/
/****************************************Insertion of queries completed****************************************************************/
		
/*******************************************************Queries Begin from here************************************************************************/
		
				/*******************Query type = 'window'*********************/	
		if(args[0].equals("window")){
			if(args[1].equals("firebuilding")){
				String lowerLeftXCoord = args[2];
				String lowerLeftYCoord = args[3];
				String topRightXCoord = args[4];
				String topRightYCoord = args[5];
			
				queryString = new StringBuilder();
				queryString.append("SELECT B.B_ID FROM building B, firebuilding FB").append(" WHERE FB.FB_NAME = B.B_NAME AND SDO_INSIDE(B.shape, SDO_GEOMETRY(2003, NULL, NULL, SDO_ELEM_INFO_ARRAY(1, 1003, 3), SDO_ORDINATE_ARRAY(");
				queryString.append(Integer.parseInt(lowerLeftXCoord)).append(",");
				queryString.append(580-Integer.parseInt(lowerLeftYCoord)).append(",");
				queryString.append(Integer.parseInt(topRightXCoord)).append(",");
				queryString.append(580-Integer.parseInt(topRightYCoord)).append("))) = 'TRUE'");
				//queryString.append(" AND B.B_NAME IN (SELECT FB.FB_NAME FROM firebuilding FB)");
				//System.out.println(queryString);
			
				statement = conn.createStatement();
				ResultSet rs = statement.executeQuery(queryString.toString());
				System.out.println("B_ID");
				System.out.println("------");
				while(rs.next()){
					String output = rs.getString("B_ID");
					System.out.println(output);
				}
				statement.close();
			}
			else if(args[1].equals("building")){
				String lowerLeftXCoord = args[2];
				String lowerLeftYCoord = args[3];
				String topRightXCoord = args[4];
				String topRightYCoord = args[5];
				
				queryString = new StringBuilder();
				queryString.append("SELECT B.B_ID FROM building B WHERE SDO_INSIDE(shape, SDO_GEOMETRY(2003, NULL, NULL, SDO_ELEM_INFO_ARRAY(1, 1003, 3), SDO_ORDINATE_ARRAY(");
				queryString.append(Integer.parseInt(lowerLeftXCoord)).append(",");
				queryString.append(580-Integer.parseInt(lowerLeftYCoord)).append(",");
				queryString.append(Integer.parseInt(topRightXCoord)).append(",");
				queryString.append(580-Integer.parseInt(topRightYCoord)).append("))) = 'TRUE'");
				queryString.append(" AND B.B_NAME NOT IN (SELECT FB.FB_NAME FROM firebuilding FB)");
				
				statement = conn.createStatement();
				ResultSet rs = statement.executeQuery(queryString.toString());
				System.out.println("B_ID");
				System.out.println("------");
				while(rs.next()){
					String output = rs.getString("B_ID");
					System.out.println(output);
				}
				statement.close();
			}
			else if(args[1].equals("firehydrant")){
				String lowerLeftXCoord = args[2];
				String lowerLeftYCoord = args[3];
				String topRightXCoord = args[4];
				String topRightYCoord = args[5];
				
				queryString = new StringBuilder();
				queryString.append("SELECT F.F_ID FROM firehydrant F WHERE SDO_INSIDE(shape, SDO_GEOMETRY(2001, NULL, NULL, SDO_ELEM_INFO_ARRAY(1, 1003, 3), SDO_ORDINATE_ARRAY(");
				queryString.append(Integer.parseInt(lowerLeftXCoord)).append(",");
				queryString.append(580 - Integer.parseInt(lowerLeftYCoord)).append(",");
				queryString.append(Integer.parseInt(topRightXCoord)).append(",");
				queryString.append(580 - Integer.parseInt(topRightYCoord)).append("))) = 'TRUE'");
				
				statement = conn.createStatement();
				ResultSet rs = statement.executeQuery(queryString.toString());
				System.out.println("F_ID");
				System.out.println("----");
				while(rs.next()){
					String output = rs.getString("F_ID");
					System.out.println(output);
				}
				statement.close();
				
			}
		}
		
	/*******************Query type = 'within'*********************/	
		if(args[0].equals("within")){
			if(args[1].equals("firebuilding")){
				String buildingName = args[2];
				String distance = args[3];
				
				queryString = new StringBuilder();
				queryString.append("SELECT B1.B_ID FROM building B1, firebuilding FB, building B2");
				queryString.append(" WHERE B1.B_NAME = FB.FB_NAME AND B1.B_NAME!='").append(buildingName).append("'");
				queryString.append(" AND B2.B_NAME = '").append(buildingName).append("'");
				queryString.append(" AND SDO_WITHIN_DISTANCE(B1.shape, B2.shape, 'distance=").append(distance).append("') = 'TRUE'");
				
				statement = conn.createStatement();
				ResultSet rs = statement.executeQuery(queryString.toString());	
				System.out.println("FB_ID");
				System.out.println("----");
				
				while(rs.next()){
					String output = rs.getString("B_ID");
					System.out.println(output);
				}
				statement.close();
				
			}
			else if(args[1].equals("building")){
				String buildingName = args[2];
				String distance = args[3];
				
				queryString = new StringBuilder();
				queryString.append("SELECT B2.B_ID FROM building B1, building B2 WHERE B1.B_NAME = '");
				queryString.append(buildingName).append("' AND B2.B_NAME !='");
				queryString.append(buildingName).append("'");
				queryString.append(" AND SDO_WITHIN_DISTANCE(B1.shape, B2.shape, 'distance=");
				queryString.append(distance).append("') = 'TRUE'");
				queryString.append(" AND B2.B_NAME NOT IN (SELECT FB.FB_NAME FROM firebuilding FB)");
				
				statement = conn.createStatement();
				ResultSet rs = statement.executeQuery(queryString.toString());
				System.out.println("FB_ID");
				System.out.println("----");
				
				while(rs.next()){
					String output = rs.getString("B_ID");
					System.out.println(output);
				}
				statement.close();
			}
			else if(args[1].equals("firehydrant")){
				String buildingName = args[2];
				String distance = args[3];
				
				queryString = new StringBuilder();
				queryString.append("SELECT F.F_ID FROM firehydrant F, building B WHERE B.B_NAME='");
				queryString.append(buildingName).append("'");
				queryString.append(" AND SDO_WITHIN_DISTANCE(F.shape, B.shape, 'distance=");
				queryString.append(distance).append("') = 'TRUE'");
				
				statement = conn.createStatement();
				ResultSet rs = statement.executeQuery(queryString.toString());
				System.out.println("F_ID");
				System.out.println("----");
				
				while(rs.next()){
					String output = rs.getString("F_ID");
					System.out.println(output);
				}
				statement.close();
				
			}
		}
		
		
				/*******************Query type = 'nn'*********************/
		if(args[0].equals("nn")){
			if(args[1].equals("building")){
				String id = args[2];
				String number_of_neighbours = args[3];
				
				queryString = new StringBuilder();
				queryString.append("SELECT B_ID FROM (SELECT B.B_ID, MDSYS.SDO_NN_DISTANCE(1)  FROM building B WHERE B.B_ID!='");
				queryString.append(id).append("'");
				queryString.append("AND B.B_NAME NOT IN (SELECT F.FB_NAME FROM firebuilding F) AND SDO_NN(B.shape, (SELECT D.shape FROM building D WHERE D.B_ID = '");
				queryString.append(id).append("'), 'SDO_BATCH_SIZE=0', 1) = 'TRUE'");
				queryString.append("ORDER BY MDSYS.SDO_NN_DISTANCE(1) ASC) WHERE ROWNUM<=").append(number_of_neighbours);
				
				statement = conn.createStatement();
				ResultSet rs = statement.executeQuery(queryString.toString());
				System.out.println("B_ID");
				System.out.println("----");
				
				while(rs.next()){
					String output = rs.getString("B_ID");
					System.out.println(output);
				}
				statement.close();
				
			}
			
			if(args[1].equals("firehydrant")){
				String id = args[2];
				String number_of_neighbours = args[3];
				
				queryString.append("SELECT F.F_ID AS ID_NO FROM BUILDING B, FIREHYDRANT F WHERE SDO_NN(F.SHAPE, B.SHAPE, 'SDO_NUM_RES="+number_of_neighbours+"') = 'TRUE'");
				queryString.append(" AND B.B_ID='"+id+"'");
								
				statement = conn.createStatement();
				ResultSet rs = statement.executeQuery(queryString.toString());
				System.out.println("F_ID");
				System.out.println("----");
				
				while(rs.next()){
					String output = rs.getString("ID_NO");
					System.out.println(output);
				}
				statement.close();
			}
			
			if(args[1].equals("firebuilding")){
				String id = args[2];
				String number_of_neighbours = args[3];
				
				queryString = new StringBuilder();
				queryString.append("SELECT * FROM(SELECT B1.B_ID FROM BUILDING B1, BUILDING B3 WHERE B3.B_ID='").append(id).append("'");
				queryString.append(" AND B1.B_ID <> B3.B_ID AND SDO_NN(B1.SHAPE, B3.SHAPE)='TRUE' AND B1.B_ID IN (SELECT B1.B_ID");
				queryString.append(" FROM BUILDING B1, FIREBUILDING F1 WHERE F1.FB_NAME=B1.B_NAME)) WHERE ROWNUM<=").append(number_of_neighbours);
				
				
				/*queryString.append("SELECT B1.B_ID FROM building B1, firebuilding FB, building B2 WHERE B1.B_NAME = FB.FB_NAME");
				queryString.append(" AND B2.B_ID != '").append(id).append("'");
				queryString.append("B2.B_NAME IN (SELECT F.FB NAME FROM firebuilding FB) AND SDO_NN(B1.shape, (SELECT D.shape from building D WHERE D.B_ID = '");
				queryString.append(id).append("'), 'SDO_BATCH_SIZE=0') = 'TRUE'");
				queryString.append(" ORDER BY MDSYS.SDO_NN_DISTANCE(1) ASC) WHERE ROWNUM<=").append(number_of_neighbours);*/
				
				statement = conn.createStatement();
				ResultSet rs = statement.executeQuery(queryString.toString());
				System.out.println("FB_ID");
				System.out.println("----");
				
				while(rs.next()){
					String output = rs.getString("B_ID");
					System.out.println(output);
				}
				statement.close();
			}
				
		}
		
											/*******Demos begin here*****/
		if(args[0].equals("demo")){
			if(args[1].equals("1")){
				queryString = new StringBuilder();
				queryString.append("SELECT B.B_NAME FROM building B WHERE B.B_NAME LIKE 'S%' AND B.B_NAME NOT IN(");
				queryString.append("SELECT FB.FB_NAME FROM firebuilding FB)");
				
				statement = conn.createStatement();
				ResultSet rs = statement.executeQuery(queryString.toString());
				System.out.println("Building Name");
				System.out.println("----");
				
				while(rs.next()){
					String output = rs.getString("B_NAME");
					System.out.println(output);
				}
				statement.close();
			}
			else if(args[1].equals("2")){
				queryString = new StringBuilder();
				/*queryString.append("SELECT B.B_NAME, F.F_ID FROM building B, firehydrant F ");
				queryString.append("WHERE b.b_name IN (SELECT fb.fb_name FROM firebuilding fb) ");
				queryString.append("AND SDO_NN(b.shape, f.shape, 'sdo_num_res=5') = 'TRUE'");*/
				
				queryString.append("SELECT B1, F1 FROM (SELECT B.B_NAME B1, F.F_ID F1 FROM BUILDING B, FIREHYDRANT F ");
				queryString.append("WHERE SDO_NN(F.SHAPE, B.SHAPE, 'SDO_NUM_RES=5')='TRUE')");
				queryString.append(" WHERE (B1, F1) NOT IN (SELECT B.B_NAME B2, F.F_ID F2 FROM BUILDING B, FIREHYDRANT F ");
				queryString.append("WHERE B.B_NAME NOT IN (SELECT FB.FB_NAME FROM FIREBUILDING FB) AND SDO_NN(F.SHAPE, B.SHAPE, 'SDO_NUM_RES=5') = 'TRUE')");
				
				statement = conn.createStatement();
				ResultSet rs = statement.executeQuery(queryString.toString());
				System.out.println("Building Name		F_ID");
				System.out.println("-----			-----");
				
				while(rs.next()){
					String outputB = rs.getString("B1");
					String outputF = rs.getString("F1");
					System.out.println(outputB+"			"+outputF);
				}
				statement.close();
			}
			
			else if(args[1].equals("3")){
				queryString = new StringBuilder();
				queryString.append("SELECT F.F_ID, COUNT(*) FROM firehydrant F, building B WHERE SDO_WITHIN_DISTANCE(B.shape, F.shape, 'distance=120') = 'TRUE'");
				queryString.append(" GROUP BY(F.F_ID) HAVING COUNT(*) = ( SELECT MAX(COUNT(*)) FROM building B1, firehydrant F1");
				queryString.append(" WHERE SDO_WITHIN_DISTANCE(B1.shape, F1.shape, 'distance=120') = 'TRUE' GROUP BY(F1.F_ID))");
				
				statement = conn.createStatement();
				ResultSet rs = statement.executeQuery(queryString.toString());
				System.out.println("F_ID");
				System.out.println("----");
				
				while(rs.next()){
					String output = rs.getString("F_ID");
					System.out.println(output);
				}
				statement.close();
			}
			else if(args[1].equals("4")){
				queryString = new StringBuilder();
				queryString.append("SELECT TEMP.F_ID, TEMP.CNT FROM (SELECT F.F_ID, COUNT(*) AS CNT FROM BUILDING B, FIREHYDRANT F ");
				queryString.append("WHERE SDO_NN(F.shape, B.shape, 'sdo_num_res=1')='TRUE' GROUP BY(F.F_ID) ORDER BY CNT DESC) TEMP");
				queryString.append(" WHERE ROWNUM<=5");
				
				statement = conn.createStatement();
				ResultSet rs = statement.executeQuery(queryString.toString());
				System.out.println("F_ID	COUNT");
				System.out.println("----	-------");
				
				while(rs.next()){
					String output = rs.getString("F_ID");
					String outputCount = rs.getString("CNT");
					System.out.println(output+"		"+outputCount);
				}
				statement.close();
			}
			else if(args[1].equals("5")){
					//X co-ordinate of MBR
				queryString = new StringBuilder();
				queryString.append("SELECT maximum FROM (SELECT MAX(SDO_GEOM.SDO_MAX_MBR_ORDINATE(B.SHAPE, M.diminfo, 1)) maximum FROM BUILDING B, USER_SDO_GEOM_METADATA M ");
				queryString.append("WHERE M.table_name = 'BUILDING' AND M.column_name = 'SHAPE' AND B.B_NAME LIKE '%HE')");
				//queryString.append("SELECT UNIQUE(SDO_GEOM.SDO_MIN_MBR_ORDINATE(t, m.diminfo, 1)) FROM (SELECT SDO_AGGR_MBR(B.SHAPE)t");
				//queryString.append(" FROM BUILDING B WHERE (B.B_NAME LIKE '%HE')), USER_SDO_GEOM_METADATA M");
				
				
				statement = conn.createStatement();
				ResultSet rs = statement.executeQuery(queryString.toString());
				System.out.println("Upper X, Y Co-ordinate");
				System.out.println("----");
				
				
				while(rs.next()){
					//Object sqlObject = rs.getObject(1);
					//rs.
					String output = rs.getString("maximum");
					//int value = Integer.parseInt(sqlObject.toString());
					//System.out.print(value + ", ");
					System.out.print(output + ", ");
				}
				
				queryString = new StringBuilder();
				queryString.append("SELECT 580-MIN(SDO_GEOM.SDO_MIN_MBR_ORDINATE(B.shape, M.diminfo, 2)) AS MINX FROM building B, USER_SDO_GEOM_METADATA M ");
				queryString.append("WHERE M.table_name = 'BUILDING' AND M.column_name = 'SHAPE' AND B.B_NAME LIKE '%HE'");
				/*queryString.append("SELECT 580-MAX(SDO_GEOM.SDO_MAX_MBR_ORDINATE(B.shape, m.diminfo, 2)) AS MAXY FROM building B, user_sdo_geom_metadata M ");
				queryString.append("WHERE M.table_name = 'BUILDING' AND M.column_name = 'SHAPE' AND B.B_NAME LIKE '%HE'");*/
				
				statement = conn.createStatement();
				rs = statement.executeQuery(queryString.toString());
				/*System.out.println("Lower Y Co-ordinate");
				System.out.println("----");*/
				
				while(rs.next()){
					//Object sqlObject = rs.getObject(1);
					String output = rs.getString("MINX");
					//System.out.print(Integer.parseInt(sqlObject.toString()));
					System.out.println(output);
					System.out.println();
				}
				
				queryString = new StringBuilder();
				queryString.append("SELECT MIN(SDO_GEOM.SDO_MIN_MBR_ORDINATE(B.shape, M.diminfo, 1)) AS MINX FROM building B, USER_SDO_GEOM_METADATA M ");
				queryString.append("WHERE M.table_name = 'BUILDING' AND column_name = 'SHAPE' AND B.B_NAME LIKE '%HE'");
				
				statement = conn.createStatement();
				rs = statement.executeQuery(queryString.toString());
				System.out.println("Lower X, Y Co-ordinate");
				System.out.println("----");
				
				while(rs.next()){
					//Object sqlObject = rs.getObject(1);
					String output = rs.getString("MINX");
					//System.out.print(Integer.parseInt(sqlObject.toString())+ ", ");
					System.out.print(output+ ", ");
				}
				
				queryString = new StringBuilder();
				queryString = new StringBuilder();
				queryString.append("SELECT 580-MAX(SDO_GEOM.SDO_MAX_MBR_ORDINATE(B.shape, m.diminfo, 2)) AS MAXY FROM building B, user_sdo_geom_metadata M ");
				queryString.append("WHERE M.table_name = 'BUILDING' AND M.column_name = 'SHAPE' AND B.B_NAME LIKE '%HE'");
				/*queryString.append("SELECT 580-MIN(SDO_GEOM.SDO_MIN_MBR_ORDINATE(B.shape, M.diminfo, 2)) AS MINX FROM building B, USER_SDO_GEOM_METADATA M ");
				queryString.append("WHERE M.table_name = 'BUILDING' AND M.column_name = 'SHAPE' AND B.B_NAME LIKE '%HE'");*/
				
				statement = conn.createStatement();
				rs = statement.executeQuery(queryString.toString());
				
				while(rs.next()){
					String output = rs.getString("MAXY");
					//Object sqlObject = rs.getObject(1);
					//System.out.print(Integer.parseInt(sqlObject.toString()));
					System.out.println(output);
				}
				
				statement.close();
			}
		}
		
/*******************************************************Queries End here************************************************************************/		
		br.close();
		fr.close();
	}
}
