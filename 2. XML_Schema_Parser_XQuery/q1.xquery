Select DISTINCT R."Reviewer"
From hw3_book_table B, hw3_review_table R , 
XMLTABLE(
          'for $book in /Book
              where $book/Price >25
                return $book'
                PASSING B.BOOK_DETAILS
                COLUMNS
                "BookTitle" VARCHAR2(50) PATH 'Title',
                "PublishDate" VARCHAR2(50) PATH 'Publish_Date') B,
XMLTABLE(
          'for $review in /REVIEW return $review'
               PASSING R.REVIEW_DETAILS
                COLUMNS
                "BookTitle" VARCHAR2(50) PATH 'BOOK_TITLE',
                "Reviewer" VARCHAR2(50) PATH 'REVIEWER') R 
WHERE R."BookTitle" = B."BookTitle"
AND  ( B."PublishDate" Like '_____08___' OR
       B."PublishDate" Like '_____09___' OR
       B."PublishDate" Like '_____10___' OR
       B."PublishDate" Like '_____11___' OR
       B."PublishDate" Like '_____12___' );
