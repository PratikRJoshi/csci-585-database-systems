import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class Xquery {
	public static void main(String args[]) throws SAXException, IOException, SQLException{
/****************************************Connecting to the Database using JDBC******************************************************/
		System.out.println("-------- Connecting to Oracle JDBC ------");
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
		}
		catch(Exception e){
			System.out.println("Where the hell is the driver to connect to??");
			e.printStackTrace();
		}
		
		System.out.println("Oracle Driver Registered");
		
		Connection conn = null;
		Statement statement;
		
		try{
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "system","Pratik");
		}
		catch(SQLException e){
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
		}
		
		if(conn!=null)
			System.out.println("Connection to the database successful");
		else
			System.out.println("Failed to make connection!");
		
/****************************************Connection to the Database using JDBC completed***********************************************/
		
/*******************************************Insertion of data from XML to database started***************************************************************/
		try {
		File file = new File("book.xml");
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		
		String line;
		StringBuilder queryString = new StringBuilder();
		queryString.append("CREATE TABLE HW3_BOOK_TABLE(BOOK_ID INT,BOOK_DETAILS XMLTYPE)");
		statement = conn.createStatement();
		statement.executeUpdate(queryString.toString());
		statement.close();
		
		line=br.readLine();
		while((line=br.readLine())!=null){
			//if(line.equalsIgnoreCase("<Books>"))
			if(line.matches("<Books(.*)>"))
				break;
		}
		
		line = "";
		DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
		Document doc = documentBuilder.parse(file);
		doc.getDocumentElement().normalize();
		
		NodeList bookNodeList = doc.getElementsByTagName("Book");
		for(int count=0; count<bookNodeList.getLength();count++){
			Node book = bookNodeList.item(count);
			if (book.getNodeType() == Node.ELEMENT_NODE){
				//System.out.println(((Element)book).getElementsByTagName("ID").item(0).getTextContent());
				//System.out.println(((Element)book).getElementsByTagName("Title").item(0).getTextContent());
				queryString = new StringBuilder();
				queryString.append("INSERT INTO HW3_Book_table VALUES(").append(count+1).append(", XMLType('");
				while((line=br.readLine())!=null){
					if(line.trim().equals("</Book>"))
						break;
					else{
						if(line.contains("'")){
							line = line.replace("'","''");
							line = line.trim();
							queryString.append(line);
						}
						else{
							line = line.trim();
							queryString.append(line);
						}
					}
				}
				line = line.trim();
				queryString.append(line).append("'))");
				//System.out.println(queryString);
				statement = conn.createStatement();
				statement.executeUpdate(queryString.toString());
				statement.close();
			}
		}
		
		br.close();
		fr.close();
		
		file = new File("review.xml");
		fr = new FileReader(file);
		br = new BufferedReader(fr);
		
		queryString = new StringBuilder();
		queryString.append("CREATE TABLE HW3_REVIEW_TABLE(REVIEW_ID INT,REVIEW_DETAILS XMLTYPE)");
		statement = conn.createStatement();
		statement.executeUpdate(queryString.toString());
		statement.close();
		
		line="";
		while((line=br.readLine())!=null){
			if(line.matches("<REVIEWS(.*)>"))
				break;
		}
		line="";
		documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		doc = documentBuilder.parse(file);
		doc.getDocumentElement().normalize();
		
		//bookNodeList = doc.getElementsByTagName("REVIEW");
		NodeList reviewNodeList = doc.getElementsByTagName("REVIEW");
		
		for(int count=0; count<reviewNodeList.getLength();count++){
			Node book = reviewNodeList.item(count);
			if (book.getNodeType() == Node.ELEMENT_NODE){
				//System.out.println(((Element)book).getElementsByTagName("ID").item(0).getTextContent());
				//System.out.println(((Element)book).getElementsByTagName("Title").item(0).getTextContent());
				queryString = new StringBuilder();
				queryString.append("INSERT INTO HW3_Review_table VALUES(").append(count+1).append(", XMLType('");
				while((line=br.readLine())!=null){
					if(line.trim().equals("</REVIEW>"))
						break;
					else{
						if(line.contains("'")){
							line = line.replace("'","''");
							line = line.trim();
							queryString.append(line);
						}
						else{
							line = line.trim();
							queryString.append(line);
						}
					}
				}
				line = line.trim();
				queryString.append(line).append("'))");
				System.out.println(queryString);
				statement = conn.createStatement();
				statement.executeUpdate(queryString.toString());
				statement.close();
			}
			
		}
		br.close();
		fr.close();
		}catch (ParserConfigurationException e){
			e.printStackTrace();
		}

	}
}
