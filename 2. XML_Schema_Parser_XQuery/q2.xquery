Select DISTINCT B."Book_ID"
From hw3_book_table B, hw3_review_table R, 
XMLTABLE(
        'for $book in /Book
        return $book'
        PASSING B.BOOK_DETAILS
        COLUMNS
            "BookTitle" VARCHAR2(50) PATH 'Title',
            "Book_ID" VARCHAR2(50) PATH 'ID') B,
XMLTABLE(
        'for $review in /REVIEW 
        where number(substring(data($review/REVIEW_DATE), 1, 4)) = 2014
        return $review'
        PASSING R.REVIEW_DETAILS
        COLUMNS
            "BookTitle" VARCHAR2(50) PATH 'BOOK_TITLE',
            "ReviewDate" VARCHAR2(50) PATH 'REVIEW_DATE') R 
WHERE B."BookTitle" = R."BookTitle"
