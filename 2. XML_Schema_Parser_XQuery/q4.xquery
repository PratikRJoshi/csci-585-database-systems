SELECT R."BookTitle", AVG(R."Rating")
FROM hw3_review_table R,
XMLTABLE(
        'for $rating in /REVIEW return $rating'
        PASSING R.REVIEW_DETAILS
        COLUMNS
              "BookTitle" VARCHAR(50) PATH 'BOOK_TITLE',
              "Rating" VARCHAR(50) PATH 'RATING') R
GROUP BY R."BookTitle"
ORDER BY AVG(R."Rating") DESC;