Select R."Reviewer" , AVG(R."Rating") AS AVERAGE_VALUE , COUNT(R."Rating") AS REVIEW_COUNT
FROM hw3_review_table R,
XMLTABLE(
        'for $review in /REVIEW return $review'
        PASSING R.REVIEW_DETAILS
        COLUMNS
            "BookTitle" VARCHAR2(50) PATH 'BOOK_TITLE',
            "Rating" VARCHAR2(50) PATH 'RATING',
            "Reviewer" VARCHAR2(50) PATH 'REVIEWER') R 
GROUP BY R."Reviewer";