Select R."ReviewerName" , R."BookTitle"
FROM hw3_review_table R,
XMLTABLE(
          'for $review in /REVIEW 
		   return $review'
           Passing R.REVIEW_DETAILS
           COLUMNS
                "BookTitle" VARCHAR2(50) PATH 'BOOK_TITLE',
                "ReviewerName" VARCHAR2(50) PATH 'REVIEWER') R 
ORDER BY R."ReviewerName" ASC , R."BookTitle" ASC;