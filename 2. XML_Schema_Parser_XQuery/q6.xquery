Select R."ReviewerName",R."Rating",R."ReviewerDate",B."PublishDate"
From hw3_book_table B, hw3_review_table R, 
XMLTABLE(
        'for $book in /Book
        return $book'
        PASSING B.BOOK_DETAILS
        COLUMNS
            "BookTitle" VARCHAR2(50) PATH 'Title',
            "PublishDate" VARCHAR2(50) PATH 'Publish_Date') B,
XMLTABLE(
        'for $review in /REVIEW 
        where $review/RATING > 3
        return $review'
        PASSING R.REVIEW_DETAILS
        COLUMNS
            "BookTitle" VARCHAR2(50) PATH 'BOOK_TITLE',
            "ReviewerDate" VARCHAR2(50) PATH 'REVIEW_DATE',
            "Rating" VARCHAR2(50) PATH 'RATING',
            "ReviewerName" VARCHAR2(50) PATH 'REVIEWER') R 
WHERE R."BookTitle" = B."BookTitle"
ORDER BY B."PublishDate" ASC,R."ReviewerDate" DESC;